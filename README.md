# Kanvas API

API para registro de: 

- ALUNOS            (is_superuser=False,    is_staff=False)
- INSTRUTORES       (is_superuser=True,     is_staff=False) 
- FACILITADORES     (is_superuser=False,    is_staff=True)

com as seguintes Models:

- User        (username=CharField, password=CharField, is_superuser=Boolean, is_staff=Boolean) __1:N Activity__
- Course      (name=CharField, user_set=lista de alunos) __N:N User__
- Activity    (repo=CharField, grade=FloatField, user, User=type_student)

## Regras das Permissões por tipo de Usuário

- Um ALUNO pode logar, postar e listar suas atividades.
- Um FACILITADOR pode logar, atualizar e listar notas de quaisquer atividades.
- Um INSTRUTOR pode logar, postar cursos, atualizar e listar notas de quaisquer atividades.
- QUALQUER um, mesmo sem logar, pode listar todos os cursos e alunos cadastrados no mesmo.

## Instalação

- python3 -m venv venv
- pip install django djangorestframework

## Utilização

### POST => /api/accounts/

* OBS: O nome de usuário deve ser único, caso já exista username cadastrado:
-- RESPONSE STATUS -> HTTP 409
```
{
    'message': 'username already registered'
}
```

- Criando um estudante; espera o seguinte JSON no body da requisição:
```
{
  "username": "student",
  "password": "1234",
  "is_superuser": false,
  "is_staff": false
}
```
-- RESPONSE STATUS -> HTTP 201
```
{
  "id": 1,
  "username": "student",
  "is_superuser": false,
  "is_staff": false
}
```

- Criando um facilitador; espera o seguinte JSON no body da requisição:
```
{
  "username": "facilitator",
  "password": "1234",
  "is_superuser": false,
  "is_staff": true
}
```
-- RESPONSE STATUS -> HTTP 201
```
{
  "id": 2,
  "username": "facilitator",
  "is_superuser": false,
  "is_staff": true
}
```
- Criando um instrutor; espera o seguinte JSON no body da requisição:
```
{
  "username": "instructor",
  "password": "1234",
  "is_superuser": true,
  "is_staff": true
}
```
-- RESPONSE STATUS -> HTTP 201
```
{
  "id": 3,
  "username": "instructor",
  "is_superuser": true,
  "is_staff": true
}
```

### POST => /api/login/

- Qualquer tipo de usuário; espera-se o seguinte JSON no body da requisição:
```
{
  "username": "student",
  "password": "1234"
}
```
-- RESPONSE STATUS -> HTTP 200
```
{
  "token": "dfd384673e9127213de6116ca33257ce4aa203cf"
}
```
ou
-- RESPONSE STATUS -> HTTP 401 - UNATHORIZED


### POST =>  /api/courses/ 

- criando um curso:
-- Header -> Authorization: Token <token-do-instrutor>
-- Espera-se o seguinte JSON no body da requisição:
```
{
  "name": "Javascript 101"
}
```
-- RESPONSE STATUS -> HTTP 201
```
{
  "id": 1,
  "name": "Javascript 101",
  "user_set": []
}
```

- Caso seja fornecido um token de estudante ou facilitador:
-- Header -> Authorization: Token <token-do-facilitador ou token-to-estudante>
-- Espera-se o seguinte JSON no body da requisição:
```
{
  "name": "Javascript 101"
}
```
-- RESPONSE STATUS -> HTTP 403
```
{
  "detail": "You do not have permission to perform this action."
}
```

### PUT => /api/courses/registrations/

- atualizando a lista de estudantes matriculados em um curso:
-- Header -> Authorization: Token <token-do-instrutor>
-- Espera-se o seguinte JSON no body da requisição:
```
{
  "course_id": 1,
  "user_ids": [1, 2, 7]
}
```
-- RESPONSE STATUS -> HTTP 200
```
{
  "id": 1,
  "name": "Javascript 101",
  "user_set": [
    {
      "id": 1,
      "is_superuser": false,
      "is_staff": false,
      "username": "luiz"
    },
    {
      "id": 7,
      "is_superuser": false,
      "is_staff": false,
      "username": "isabela"
    },
    {
      "id": 2,
      "is_superuser": false,
      "is_staff": false,
      "username": "raphael"
    }
  ]
}
```

__OBS:__ Sempre atualizará a lista de alunos matriculados no curso. No primeiro exemplo os alunos 1, 2 e 7 foram vinculados ao curso 1. Já na segunda requisição a lista de alunos foi atualizada, matriculando somente o aluno 1.

- Caso seja fornecido token de estudante ou facilitador:

// Header -> Authorization: Token <token-do-facilitador ou token-to-estudante>
-- Espera-se o seguinte JSON no body da requisição:
```
{
  "course_id": 1,
  "user_ids": [1]
}
```
-- RESPONSE STATUS -> HTTP 403
```
{
  "detail": "You do not have permission to perform this action."
}
```

### GET => /api/courses/ 

- obtendo a lista de cursos e alunos:
Este endpoint pode ser acessado por qualquer client (mesmo sem autenticação). A resposta do servidor deve trazer uma lista de cursos, mostrando cada aluno inscrito, no seguinte formato:

-- RESPONSE STATUS -> HTTP 200
```
[
  {
    "id": 1,
    "name": "Javascript 101",
    "user_set": [
      {
        "id": 1,
        "is_superuser": false,
        "is_staff": false,
        "username": "luiz"
      }
    ]
  },
  {
    "id": 2,
    "name": "Python 101",
    "user_set": []
  }
]
```

### POST => /api/activities/ 

- Criando uma atividade (estudante)

* OBS: O "repo" deve ser único, caso já exista repo cadastrado:
-- RESPONSE STATUS -> HTTP 409.

* Mesmo que o User do tipo estudante faça um request que tem o campo "grade", a nota não deve ser registrada no momento da criação.

-- Header -> Authorization: Token <token-do-estudante>
-- Espera-se o seguinte JSON no body da requisição:
```
{
  "repo": "gitlab.com/cantina-kenzie",
  "grade": 10 "Esse campo é opcional"
}
```
-- RESPONSE STATUS -> HTTP 201
-- Repare que o campo grade foi ignorado
```
{
  "id": 6,
  "user_id": 7,
  "repo": "gitlab.com/cantina-kenzie",
  "grade": null
}
```

### PUT => /api/activities/ 

- editando a nota de uma atividade (facilitador ou instrutor):
-- Header -> Authorization: Token <token-do-facilitador ou instrutor>
-- Espera-se o seguinte JSON no body da requisição:
```
{
  "id": 6,
  "grade": 10
}
```
-- RESPONSE STATUS -> HTTP 201
```
{
  "id": 6,
  "user_id": 7,
  "repo": "gitlab.com/cantina-kenzie",
  "grade": 10
}
```

* OBS: Caso seja informado o id de uma atividade inválido o sistema deverá responder com HTTP 404 - Not Found.

### => GET /api/activities/ 
- listando atividades (estudante)
-- Header -> Authorization: Token <token-do-estudante>
-- Espera-se o seguinte JSON no body da requisição:
```
[
  {
    "id": 1,
    "user_id": 1,
    "repo": "github.com/luiz/cantina",
    "grade": null
  },
  {
    "id": 6,
    "user_id": 1,
    "repo": "github.com/hanoi",
    "grade": null
  },
  {
    "id": 15,
    "user_id": 1,
    "repo": "github.com/foodlabs",
    "grade": null
  },
]
```

### GET =>  /api/activities/ 
- listando atividades (facilitador ou instrutor)
-- Header -> Authorization: Token <token-do-facilitador ou token-do-instrutor>
-- Espera-se o seguinte JSON no body da requisição:
```
[
  {
    "id": 1,
    "user_id": 1,
    "repo": "github.com/luiz/cantina",
    "grade": null
  },
  {
    "id": 6,
    "user_id": 1,
    "repo": "github.com/hanoi",
    "grade": null
  },
  {
    "id": 10,
    "user_id": 2,
    "repo": "github.com/foodlabs",
    "grade": null
  },
  {
    "id": 35,
    "user_id": 3,
    "repo": "github.com/kanvas",
    "grade": null
  },
]
```

### GET => /api/activities/<int:user_id>/ 
- filtrando atividades fornecendo um user_id de estudante opcional (facilitador ou instrutor)

-- Header -> Authorization: Token <token-do-facilitador ou token-do-instrutor>
-- REQUEST (/api/activities/1/)
```
[
  {
    "id": 1,
    "user_id": 1,
    "repo": "github.com/luiz/cantina",
    "grade": null
  },
  {
    "id": 6,
    "user_id": 1,
    "repo": "github.com/hanoi",
    "grade": null
  },
  {
    "id": 15,
    "user_id": 1,
    "repo": "github.com/foodlabs",
    "grade": null
  },
]
```