from django.shortcuts                   import get_object_or_404, get_list_or_404

from rest_framework.authentication      import TokenAuthentication
from rest_framework.permissions         import IsAuthenticatedOrReadOnly, IsAuthenticated

from rest_framework.views               import APIView
from rest_framework.response            import Response
from rest_framework                     import status

from .serializers import (
    UserSerializer, 
    AuthenticationSerializer, 
    CourseSerializer, 
    ActivitySerializer
    )
from .services      import LoginServices, UserServices, CourseServices, ActivityServices
from .models        import User, Course, Activity
from .permissions   import SuperUserPermission, ActivityPermission

STATUS = {
    'CREATED': status.HTTP_201_CREATED,
    'OK': status.HTTP_200_OK,
    'BAD_REQUEST': status.HTTP_400_BAD_REQUEST,
    'NOT_FOUND': status.HTTP_404_NOT_FOUND,
    'CONFLICT': status.HTTP_409_CONFLICT,
    'UNAUTHORIZED': status.HTTP_401_UNAUTHORIZED,
    'FORBIDDEN': status.HTTP_403_FORBIDDEN
}

class AccountView(APIView):
    
    def post(self, request):
        
        serializer = UserSerializer(data=request.data)
        
        if not serializer.is_valid():
            
            return Response(serializer.errors, status=STATUS['BAD_REQUEST'])
        
        
        new_user = UserServices.create_user_or_409(request.data)
        
        if new_user == 409:
            
            return Response({'message': 'username already registered'}, status=STATUS['CONFLICT'])
        
          
        serializer = UserSerializer(new_user)
        
        return Response(serializer.data, status=STATUS['CREATED'])   

class LoginView(APIView):
    
    def post(self, request):
        
        serializer = AuthenticationSerializer(data=request.data)
        
        if not serializer.is_valid():
            
            return Response(serializer.errors, status=STATUS['BAD_REQUEST'])
        
        token = LoginServices.get_token_or_401(request.data)
        
        if token == 401:
            
            return Response(status=STATUS['UNAUTHORIZED'])
        
        return Response({'token': token.key}, status=STATUS['OK'])
    
class CourseView(APIView):
    
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticatedOrReadOnly, SuperUserPermission]
    
    def get(self, request):
        
        all_courses = get_list_or_404(Course)
        courses = [CourseSerializer(course).data for course in all_courses]
                    
        return Response(courses, status=STATUS['OK'])
    
    def post(self, request):
        
        serializer = CourseSerializer(data=request.data)
        
        if not serializer.is_valid():
            
            return Response(serializer.errors, status=STATUS['BAD_REQUEST'])
        
        new_course = Course.objects.create(**request.data)
        serializer = CourseSerializer(new_course)
        
        return Response(serializer.data, status=STATUS['CREATED'])
    
    def put(self, request):
        
        students_list = User.objects.all()
        students_ids = request.data['user_ids']
        this_course_id = request.data['course_id']
        
        this_course = get_object_or_404(Course, id=this_course_id)
        serializer_to_update = CourseSerializer(this_course).data
        
        CourseServices.update_students_list(serializer_to_update, students_ids, this_course, students_list)
            
        serializer = CourseSerializer(this_course)
        
        return Response(serializer.data, status=STATUS['OK'])

class ActivityView(APIView):
    
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated, ActivityPermission]
    
    def get(self, request, user_id = None):
        
        this_user = request.user
        
        is_student = user_id == None and (not this_user.is_superuser and not this_user.is_staff)
        is_facilitator_or_instructor_and_get_all = user_id == None and (this_user.is_superuser or this_user.is_staff)
        is_facilitator_or_instructor_and_get_by_student_id = user_id != None and (this_user.is_superuser or this_user.is_staff)
        
        if is_student:
            
            this_activities = get_list_or_404(Activity)
            activities_serializer = ActivityServices.get_and_serializer_acitivities(ActivitySerializer, this_activities, this_user)
            
            return Response(activities_serializer, status=STATUS['OK'])
        
        if is_facilitator_or_instructor_and_get_all:
            
            this_activities = Activity.objects.all()
            activities_list = [ActivitySerializer(activity).data for activity in this_activities]
            
            return Response(activities_list, status=STATUS['OK'])
        
        if is_facilitator_or_instructor_and_get_by_student_id:
            
            try:
            
                this_activities = get_list_or_404(Activity)
                this_student = get_object_or_404(User, id=user_id)

                activities_serializer = ActivityServices.get_and_serializer_acitivities(ActivitySerializer, this_activities, this_student)
                
                return Response(activities_serializer, status=STATUS['OK'])
            
            except:
                
                return Response({"detail": "Invalid user_id."}, status=STATUS['NOT_FOUND'])
    
    def post(self, request):
        
        serializer = ActivitySerializer(data=request.data)
        
        if not serializer.is_valid():
            
            return Response(serializer.errors, status=STATUS['BAD_REQUEST'])
        
        
        this_activity = ActivityServices.post_activity_or_409(request.data, request.user)
        
        if this_activity == 409:
            
            return Response({"message": "Activity already posted"}, status=STATUS['CONFLICT'])
        
        serializer = ActivitySerializer(this_activity).data
        serializer['user_id'] = request.user.id
        
        return Response(serializer, status=STATUS['CREATED'])
    
    def put(self, request):
        
        this_activity = ActivityServices.update_object_or_404(request.data)
        
        if this_activity == 404:
            
            return Response({"message": "Not found"}, status=STATUS['NOT_FOUND'])
        
          
        serializer = ActivitySerializer(this_activity).data

        return Response(serializer, status=STATUS['CREATED'])