from rest_framework.permissions import BasePermission

class SuperUserPermission(BasePermission):
    
    def has_permission(self, request, view):
        
        if request.method == 'GET':
            return True
        
        return request.user.is_superuser

class ActivityPermission(BasePermission):
    
    def has_permission(self, request, view):
        
        if request.method == 'PUT':
            
            return request.user.is_superuser or request.user.is_staff
        
        if request.method == 'POST':
        
            return not request.user.is_superuser and not request.user.is_staff
        
        if request.method == 'GET':
            
            return True