from .models import User, Course
from rest_framework import serializers

class UserSerializer(serializers.Serializer):
    
    id = serializers.IntegerField(read_only=True)
    username = serializers.CharField()
    is_superuser = serializers.BooleanField()
    is_staff = serializers.BooleanField()
    
class AuthenticationSerializer(serializers.Serializer):
    
    username = serializers.CharField()
    password = serializers.CharField()
    
class CourseSerializer(serializers.Serializer):
    
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField()
    user_set = UserSerializer(many=True, required=False)
    
class ActivitySerializer(serializers.Serializer):
    
    id = serializers.IntegerField(read_only=True)
    user_id = serializers.IntegerField(read_only=True)
    repo = serializers.CharField()
    grade = serializers.FloatField(required=False)