from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    
    username = models.CharField(max_length=255, unique=True)
    
class Course(models.Model):
    
    name = models.CharField(max_length=255)
    user_set = models.ManyToManyField(User, blank=True)
    
class Activity(models.Model):
    
    repo = models.CharField(max_length=255, unique=True)
    grade = models.FloatField(blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)