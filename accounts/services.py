from django.db                          import IntegrityError
from django.contrib.auth                import authenticate
from rest_framework.authtoken.models    import Token
from .models                            import User, Activity

class LoginServices():
    
    @staticmethod
    def get_token_or_401(user_credentials):
        
        user = authenticate(
            username=user_credentials['username'],
            password=user_credentials['password']
        )
        
        if not user:
            
            return 401
            
        return Token.objects.get_or_create(user=user)[0]

class UserServices():
    
    @staticmethod
    def create_user_or_409(user_data):
        
        try:
    
            new_user = User.objects.create_user(
                username=user_data['username'],
                password=user_data['password'],
                is_superuser=user_data['is_superuser'],
                is_staff=user_data['is_staff']
                )
            
            return new_user
    
        except IntegrityError as error:
            
            if 'UNIQUE constrain' in error.args[0]:
                
                return 409

class CourseServices():
    
    @staticmethod
    def update_students_list(serializer_to_update, students_ids, this_course, students_list):
        
        CourseServices.remove_all_students(serializer_to_update, this_course, students_list)
        CourseServices.add_this_students(students_ids, this_course, students_list)
    
    @staticmethod
    def remove_all_students(serializer_to_update, this_course, students_list):
        
        for student in serializer_to_update['user_set']:
    
            try:
                
                this_student = [student_to_remove for student_to_remove in students_list if student_to_remove.id == student['id']][0]
                this_course.user_set.remove(this_student)
                
            except:
                
                pass
            
    @staticmethod
    def add_this_students(course_data, this_course, students_list):
        
        for student_id in course_data:
    
            this_student = [student for student in students_list if student.id == student_id][0]
            this_course.user_set.add(this_student)

class ActivityServices():
    
    @staticmethod
    def get_and_serializer_acitivities(serializer_callback, activities_list, user):
        
        return [
            serializer_callback(activity).data 
            for activity in activities_list 
            if activity.user.id == user.id
            ]
    
    @staticmethod
    def post_activity_or_409(activity, student):
        
        try:
            
            activity['grade'] = None
            new_actvity = Activity.objects.create(**activity, user=student)
            
            return new_actvity
        
        except IntegrityError as error:
    
            if 'UNIQUE constrain' in error.args[0]:
                
                return 409
    
    @staticmethod
    def update_object_or_404(activity):
        
        try:
            
            this_activity = Activity.objects.get(id=activity['id'])
            this_activity.grade = activity['grade']
            this_activity.save()
            
            return this_activity
            
        except:
            
            return 404
            